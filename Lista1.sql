CREATE TABLE BANCO(
    CODIGO CHAR(4) PRIMARY KEY NOT NULL,
    NOME VARCHAR2(50) UNIQUE NOT NULL
);

CREATE TABLE PAIS(
    ID_PAIS INT PRIMARY KEY NOT NULL,
    NOME VARCHAR2(50) UNIQUE NOT NULL
);

CREATE TABLE ESTADO(
    ID_ESTADO INT PRIMARY KEY NOT NULL,
    ID_PAIS INT NOT NULL,
    NOME VARCHAR2(50) UNIQUE NOT NULL,
    CONSTRAINT FK_ID_PAIS FOREIGN KEY ( ID_PAIS ) REFERENCES PAIS( ID_PAIS )
);

CREATE TABLE CIDADE(
    ID_CIDADE INT NOT NULL,
    ID_ESTADO INT NOT NULL,
    NOME VARCHAR2(50) NOT NULL,
    CONSTRAINT PK_ID_CIDADE_ESTADO PRIMARY KEY ( ID_CIDADE, ID_ESTADO ),
    CONSTRAINT FK_ID_ESTADO FOREIGN KEY ( ID_ESTADO ) REFERENCES ESTADO( ID_ESTADO )
);

CREATE TABLE BAIRRO(
    ID_BAIRRO INT NOT NULL,
    ID_CIDADE INT NOT NULL,
    ID_ESTADO INT NOT NULL,
    NOME VARCHAR2(50) NOT NULL,
    CONSTRAINT PK_ID_BAIRRO_CIDADE PRIMARY KEY ( ID_BAIRRO, ID_CIDADE ),
    CONSTRAINT FK_ID_CIDADE_ESTADO FOREIGN KEY ( ID_CIDADE, ID_ESTADO ) REFERENCES CIDADE( ID_CIDADE, ID_ESTADO )
);

CREATE TABLE ENDERECO(
    ID_ENDERECO INT PRIMARY KEY NOT NULL,
    ID_BAIRRO INT NOT NULL,
    ID_CIDADE INT NOT NULL,
    LOGRADOURO VARCHAR2(255) NOT NULL,
    NUMERO INT NOT NULL,
    COMPLEMENTO VARCHAR2(100) NULL,
    CEP CHAR(9) NULL,
    CONSTRAINT FK_ID_BAIRRO_CIDADE FOREIGN KEY ( ID_BAIRRO, ID_CIDADE ) REFERENCES BAIRRO( ID_BAIRRO, ID_CIDADE )
);

CREATE TABLE AGENCIA(
    CODIGO CHAR(6) NOT NULL,
    ID_BANCO CHAR(4) NOT NULL,
    ID_ENDERECO INT NOT NULL,
    NOME VARCHAR2(100) NOT NULL,
    CONSTRAINT PK_CODIGO_ID_BANCO PRIMARY KEY ( CODIGO, ID_BANCO ),
    CONSTRAINT FK_ID_BANCO FOREIGN KEY ( ID_BANCO ) REFERENCES BANCO( CODIGO ),
    CONSTRAINT FK_ID_ENDERECO FOREIGN KEY ( ID_ENDERECO ) REFERENCES ENDERECO( ID_ENDERECO )
);

CREATE TABLE CONSOLIDACAO(
    ID_CONSOLIDACAO INT PRIMARY KEY NOT NULL,
    FK_CODIGO CHAR(6) NOT NULL,
    ID_BANCO CHAR(4) NOT NULL,
    SALDO_ATUAL DECIMAL(18,2) NOT NULL,
    SAQUES DECIMAL(18,2) NOT NULL,
    DEPOSITOS DECIMAL(18,2) NOT NULL,
    NUMERO_CORRENTISTA INT NOT NULL,
    CONSTRAINT FK_CODIGO_ID_BANCO FOREIGN KEY ( FK_CODIGO, ID_BANCO ) REFERENCES AGENCIA( CODIGO, ID_BANCO )
);

CREATE TABLE TIPO_CONTA(
    ID_TIPO_CONTA INT PRIMARY KEY NOT NULL,
    NOME VARCHAR2(20) NOT NULL
);

CREATE TABLE CONTA(
    CODIGO INT NOT NULL,
    CODIGO_AGENCIA CHAR(6) NOT NULL,
    ID_BANCO CHAR(4) NOT NULL,
    TIPO_CONTA INT NOT NULL,
    SALDO DECIMAL(18,2) DEFAULT 0.0 NOT NULL,
    CONSTRAINT PK_CODIGO PRIMARY KEY ( CODIGO ),
    CONSTRAINT FK_TIPO_CONTA FOREIGN KEY ( TIPO_CONTA ) REFERENCES TIPO_CONTA( ID_TIPO_CONTA ),
    CONSTRAINT FK_CODIGO_AGENCIA FOREIGN KEY ( CODIGO_AGENCIA, ID_BANCO ) REFERENCES AGENCIA( CODIGO, ID_BANCO )
);

CREATE TABLE MOVIMENTACOES(
    ID_MOVIMENTACOES INT PRIMARY KEY NOT NULL,
    CODIGO INT NOT NULL,
    TIPO VARCHAR2(10) CHECK( TIPO IN ('D�BITO', 'CR�DITO') ),
    VALOR DECIMAL(18,2) NOT NULL,
    CONSTRAINT FK_CODIGO_CONTA FOREIGN KEY ( CODIGO ) REFERENCES CONTA( CODIGO )
);

CREATE TABLE CLIENTE(
    CPF CHAR(14) PRIMARY KEY NOT NULL,
    NOME VARCHAR2(100) NOT NULL,
    DATA_NASCIMENTO CHAR(10) NOT NULL,
    ESTADO_CIVIL VARCHAR2(20) CHECK( ESTADO_CIVIL IN ('SOLTEIRX', 'CASADX', 'DIVORCIADX', 'UNI�O EST�VEL', 'VIUVX') )
);

CREATE TABLE CLIENTE_X_ENDERECO(
    ID_CLIENTE CHAR(14) NOT NULL,
    ID_ENDERECO INT NOT NULL,
    CONSTRAINT PK_ID_CLIENTE_ENDERECO PRIMARY KEY ( ID_CLIENTE, ID_ENDERECO ),
    CONSTRAINT FK_ID_ENDERECO_2 FOREIGN KEY ( ID_ENDERECO ) REFERENCES ENDERECO( ID_ENDERECO ),
    CONSTRAINT FK_ID_CLIENTE FOREIGN KEY ( ID_CLIENTE ) REFERENCES CLIENTE( CPF )
);

CREATE TABLE CLIENTE_X_CONTA(
    ID_CLIENTE CHAR(14) NOT NULL,
    CODIGO INT NOT NULL,
    ID_BANCO CHAR(4) NOT NULL,
    TIPO_CONTA INT NOT NULL,
    CONSTRAINT PK_CLIENTE_BANCO_TIPO PRIMARY KEY ( ID_CLIENTE, ID_BANCO, TIPO_CONTA ),
    CONSTRAINT FK_ID_BANCO_2 FOREIGN KEY ( ID_BANCO ) REFERENCES BANCO( CODIGO ),
    CONSTRAINT FK_CODIGO_CONTA_2 FOREIGN KEY ( CODIGO ) REFERENCES CONTA( CODIGO ),
    CONSTRAINT FK_TIPO_CONTA_2 FOREIGN KEY ( TIPO_CONTA ) REFERENCES TIPO_CONTA( ID_TIPO_CONTA ),
    CONSTRAINT FK_ID_CLIENTE_2 FOREIGN KEY ( ID_CLIENTE ) REFERENCES CLIENTE( CPF )
);

CREATE TABLE GERENTE(
    CPF CHAR(14) PRIMARY KEY NOT NULL,
    FK_CODIGO CHAR(6) NOT NULL,
    ID_BANCO CHAR(4) NOT NULL,
    CODIGO_FUNCIONARIO INT NOT NULL,
    TIPO_GERENTE CHAR(2) CHECK( TIPO_GERENTE IN ('GC', 'GG') ), 
    CONSTRAINT FK_ID_CLIENTE_3 FOREIGN KEY ( CPF ) REFERENCES CLIENTE( CPF ),
    CONSTRAINT FK_CODIGO_ID_BANCO_2 FOREIGN KEY ( FK_CODIGO, ID_BANCO ) REFERENCES AGENCIA( CODIGO, ID_BANCO )
);


CREATE TABLE GERENTE_X_CONTA(
    CPF CHAR(14) NOT NULL,
    CODIGO INT NOT NULL,
    CONSTRAINT PK_CPF_CODIGO_CONTA PRIMARY KEY ( CPF, CODIGO ),
    CONSTRAINT FK_CODIGO_CONTA_3 FOREIGN KEY ( CODIGO ) REFERENCES CONTA( CODIGO ),
    CONSTRAINT FK_ID_CLIENTE_4 FOREIGN KEY ( CPF ) REFERENCES CLIENTE( CPF )
);

INSERT INTO BANCO (CODIGO, NOME) VALUES ( '011', 'BANCO ALFA' );
INSERT INTO BANCO (CODIGO, NOME) VALUES ( '241', 'BANCO BETA' );
INSERT INTO BANCO (CODIGO, NOME) VALUES ( '307', 'BANCO OMEGA' );

INSERT INTO PAIS (ID_PAIS, NOME) VALUES ( 1, 'BRASIL');
INSERT INTO ESTADO (ID_ESTADO, ID_PAIS, NOME) VALUES ( 1, 1, 'NA' );
INSERT INTO CIDADE (ID_CIDADE, ID_ESTADO, NOME) VALUES ( 1, 1, 'NA' );
INSERT INTO BAIRRO (ID_BAIRRO, ID_CIDADE, ID_ESTADO, NOME) VALUES ( 1, 1, 1, 'NA' );
INSERT INTO ENDERECO (ID_ENDERECO, ID_BAIRRO, ID_CIDADE, LOGRADOURO, NUMERO, COMPLEMENTO)
        VALUES ( 1, 1, 1, 'RUA TESTANDO', 55, 'LOJA 1' );
        
        
INSERT INTO AGENCIA( CODIGO, ID_BANCO, ID_ENDERECO, NOME ) VALUES ( '0001', '011', 1, 'WEB' );
INSERT INTO AGENCIA( CODIGO, ID_BANCO, ID_ENDERECO, NOME ) VALUES ( '0002', '011', 1, 'WEB' );
INSERT INTO AGENCIA( CODIGO, ID_BANCO, ID_ENDERECO, NOME ) VALUES ( '0101', '011', 1, 'WEB' );

SELECT PAIS.NOME AS PAIS,
        BB.CODIGO,
        BB.NOME AS BANCO
FROM BANCO BB 
INNER JOIN AGENCIA AG ON BB.CODIGO = AG.ID_BANCO
INNER JOIN ENDERECO ENDER ON ENDER.ID_ENDERECO = AG.ID_ENDERECO
INNER JOIN CIDADE CITY ON CITY.ID_CIDADE = ENDER.ID_CIDADE
INNER JOIN ESTADO ON CITY.ID_ESTADO = ESTADO.ID_ESTADO
INNER JOIN PAIS ON PAIS.ID_PAIS = ESTADO.ID_PAIS
WHERE PAIS.NOME like '%AS%';

SELECT *
    FROM AGENCIA 
    INNER JOIN CONTA ON AGENCIA.CODIGO = CONTA.CODIGO_AGENCIA
    INNER JOIN CLIENTE_X_CONTA CXC ON CXC.CODIGO = CONTA.CODIGO
    INNER JOIN CLIENTE ON CLIENTE.CPF = CXC.ID_CLIENTE
    INNER JOIN CLIENTE_X_ENDERECO CXE ON CLIENTE.CPF = CXE.ID_CLIENTE
    INNER JOIN ENDERECO ON CXE.ID_ENDERECO = ENDERECO.ID_ENDERECO
    INNER JOIN ENDERECO END_AGENCIA ON AGENCIA.ID_ENDERECO = END_AGENCIA.ID_ENDERECO;
    
SELECT *
    FROM CLIENTE
    INNER JOIN CLIENTE_X_ENDERECO CXE ON CLIENTE.CPF = CXE.ID_CLIENTE
    INNER JOIN ENDERECO ON CXE.ID_ENDERECO = ENDERECO.ID_ENDERECO;
    

SELECT CODIGO, CODIGO_AGENCIA, ID_BANCO, TIPO_CONTA, SALDO FROM CONTA;

SELECT * FROM CLIENTE;

SELECT CPF FROM CLIENTE;

SELECT ID_CLIENTE, CODIGO, ID_BANCO, TIPO_CONTA FROM CLIENTE_X_CONTA;

/*begin
 for deleta in (select sequence_name, 'DROP SEQUENCE '||sequence_name||' ' AS dropar from user_sequences) loop
 BEGIN
  EXECUTE IMMEDIATE deleta.dropar;
  dbms_output.put_line('DROP SEQUENCE '||deleta.sequence_name||' ;');
  EXCEPTION WHEN OTHERS THEN
  dbms_output.put_line('Erro ao tentar dropar a tabela:'||deleta.sequence_name);
 END;
 end loop;
end;

begin
 for deleta in (select table_name, 'DROP TABLE '||table_name||' cascade constraints' AS dropar from user_tables) loop
 BEGIN
  EXECUTE IMMEDIATE deleta.dropar;
  dbms_output.put_line('DROP TABLE '||deleta.table_name||' cascade constraints;');
  EXCEPTION WHEN OTHERS THEN
  dbms_output.put_line('Erro ao tentar dropar a tabela:'||deleta.table_name);
 END;
 end loop;
end; */

